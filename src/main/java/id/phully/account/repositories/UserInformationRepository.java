package id.phully.account.repositories;

import id.phully.account.models.UserInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserInformationRepository extends JpaRepository<UserInformation, String> {
    UserInformation findByUserId(String userID);
}
