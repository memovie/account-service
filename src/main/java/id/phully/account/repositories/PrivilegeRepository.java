package id.phully.account.repositories;

import id.phully.account.models.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrivilegeRepository extends JpaRepository<Privilege, String> {
    Privilege findByName(String privilegeName);
}
