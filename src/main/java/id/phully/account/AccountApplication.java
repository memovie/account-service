package id.phully.account;

import id.phully.account.models.Privilege;
import id.phully.account.models.Role;
import id.phully.account.models.User;
import id.phully.account.models.UserInformation;
import id.phully.account.repositories.PrivilegeRepository;
import id.phully.account.repositories.RoleRepository;
import id.phully.account.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@EnableDiscoveryClient
public class AccountApplication implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Autowired
    @Qualifier("passwordEncoder")
    private PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(AccountApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {




        /*
        Role admin = new Role("Administrator");

        Privilege view = new Privilege("View");

        User andri = new User("admin", passwordEncoder.encode("admin"), true);

        UserInformation andriInformation = new UserInformation("Muhammad","Andri", "https://firebasestorage.googleapis.com/v0/b/mechat-f3ab1.appspot.com/o/81845.jpg?alt=media&token=99463e54-c195-472c-bf35-3801a51aeb86");

        admin.getPrivileges().add(view);
        view.getRoles().add(admin);

        andri.setUserInformation(andriInformation);
        andriInformation.setUser(andri);

        andri.getRoles().add(admin);
        admin.getUsers().add(andri);

        userRepository.save(andri);
        roleRepository.save(admin);
        */

    }

}

