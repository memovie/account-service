package id.phully.account.controllers;

import id.phully.account.models.Privilege;
import id.phully.account.models.Role;
import id.phully.account.models.User;
import id.phully.account.models.UserInformation;
import id.phully.account.repositories.PrivilegeRepository;
import id.phully.account.repositories.RoleRepository;
import id.phully.account.repositories.UserInformationRepository;
import id.phully.account.repositories.UserRepository;
import id.phully.account.utilities.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/public/")
public class PublicController {

    @Autowired
    UserInformationRepository userInformationRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Autowired
    @Qualifier("passwordEncoder")
    private PasswordEncoder passwordEncoder;

    @GetMapping({"/{id}"})
    public Map<String, String> publicInformation(@PathVariable("id") String id) {
        if(userRepository.existsById(id)){
            UserInformation userInformation = userInformationRepository.findByUserId(id);
            Map<String, String> userInformationMap = new HashMap<>();
            userInformationMap.put("firstName", userInformation.getFirstName());
            userInformationMap.put("lastName", userInformation.getLastName());
            userInformationMap.put("thumbnail", userInformation.getThumbnail());
            return userInformationMap;
        } else {
            throw new ResourceNotFoundException("Not Found");
        }

    }

}
