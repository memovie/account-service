package id.phully.account.controllers;

import id.phully.account.models.Role;
import id.phully.account.models.User;
import id.phully.account.repositories.RoleRepository;
import id.phully.account.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/admin/")
public class AdminController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @ResponseBody
    public Page<User> getAllUser(Pageable pageable) {
            return userRepository.findAll(pageable);
    }

    @RequestMapping(value = "/roles", method = RequestMethod.GET)
    @ResponseBody
    public Page<Role> getAllRole(Pageable pageable) {
        return roleRepository.findAll(pageable);
    }

}
