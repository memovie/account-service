package id.phully.account.controllers;

import id.phully.account.models.Role;
import id.phully.account.models.User;
import id.phully.account.repositories.PrivilegeRepository;
import id.phully.account.repositories.RoleRepository;
import id.phully.account.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/user/")
public class UserContoller {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @RequestMapping(value = "/information", method = RequestMethod.GET)
    @ResponseBody
    public User currentUserInformation(Authentication authentication) {
        if(userRepository.existsByUsername(authentication.getName())){
            return userRepository.findByUsername(authentication.getName());
        } else{
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Not Allow");
        }
    }

    @PostMapping({"/registration"})
    public void store(User user) {
        if(userRepository.existsById(user.getId())){
            throw new ResponseStatusException(HttpStatus.CONFLICT, "user already exists");
        } else {
            Role role = roleRepository.findByName("User");
            User userMap = user;
            userMap.setEnabled(false);
            userMap.getRoles().add(role);
            userRepository.save(userMap);
        }
    }

}
